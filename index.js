const fetch = require('node-fetch');

const TELEGRAM_BOT_API_TOKEN = process.env.TELEGRAM_BOT_API_TOKEN;
const INTERVAL = JSON.parse(process.env.INTERVAL) * 60e3;
if (typeof INTERVAL !== 'number') {
  throw new Error('INTERVAL should be a number');
}
const CHAT_ID = process.env.CHAT_ID;

const EXCERCISES = process.env.EXCERCISES.split(',').map(s => s.trim());
const NUM_EXCERCISES = JSON.parse(
  process.env.NUM_EXCERCISES || Math.min(EXCERCISES.length, 2).toString()
);
if (typeof NUM_EXCERCISES !== 'number') {
  throw new Error('NUM_EXCERCISES should be a number');
}
if (NUM_EXCERCISES > EXCERCISES.length) {
  throw new Error('NUM_EXCERCISES exceeds length of EXCERCISES');
}

const START_HOUR = JSON.parse(process.env.START_HOUR || '8');
if (typeof START_HOUR !== 'number') {
  throw new Error('START_HOUR should be a number');
}
const STOP_HOUR = JSON.parse(process.env.STOP_HOUR || '22');
if (typeof STOP_HOUR !== 'number') {
  throw new Error('STOP_HOUR should be a number');
}

function sendMessage(text) {
  fetch(`https://api.telegram.org/bot${TELEGRAM_BOT_API_TOKEN}/sendMessage`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({ chat_id: CHAT_ID, text })
  }).catch(function(err) {
    console.error(err);
    process.exit(1);
  });
}

function formatExcercises(excercises) {
  const selection = [];
  while (selection.length < NUM_EXCERCISES) {
    const option = excercises[Math.floor(Math.random() * excercises.length)];
    if (!selection.includes(option)) {
      selection.push(option);
    }
  }

  console.log(selection);

  return (
    `Time for your exercises 🕺\n` + selection.map(e => ' - ' + e).join('\n')
  );
}

function send() {
  const h = new Date().getHours();
  if (h >= START_HOUR && h <= STOP_HOUR) {
    const message = formatExcercises(EXCERCISES);
    console.log('sending message:', message);
    sendMessage(message);
  }
}

send();
setInterval(send, INTERVAL);
